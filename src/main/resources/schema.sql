DROP TABLE IF EXISTS `transaction`;
DROP TABLE IF EXISTS `budget`;
DROP TABLE IF EXISTS `category`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`password` VARCHAR(255) NULL DEFAULT NULL,
	`username` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `category` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`user_id` INT(11) NULL DEFAULT NULL,
	`deleted` BIT(1) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `FKpfk8djhv5natgshmxiav6xkpu` (`user_id`),
	CONSTRAINT `FKpfk8djhv5natgshmxiav6xkpu` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE `budget` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`end` DATE NULL DEFAULT NULL,
	`start` DATE NULL DEFAULT NULL,
	`tracking_period` INT(11) NULL DEFAULT NULL,
	`volume` DOUBLE NOT NULL,
	`category_id` INT(11) NULL DEFAULT NULL,
	`user_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FKrme3v2iww4j9qkxv4f93vv4mt` (`category_id`),
	INDEX `FKkuh8cj1roovp9nh6ut2igrxm2` (`user_id`),
	CONSTRAINT `FKkuh8cj1roovp9nh6ut2igrxm2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
	CONSTRAINT `FKrme3v2iww4j9qkxv4f93vv4mt` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
);

CREATE TABLE `transaction` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`amount` DOUBLE NOT NULL,
	`comment` VARCHAR(255) NULL DEFAULT NULL,
	`internal` BIT(1) NOT NULL,
	`status` INT(11) NULL DEFAULT NULL,
	`template` BIT(1) NOT NULL DEFAULT false,
	`time` DATETIME(6) NULL DEFAULT NULL,
	`budget_id` INT(11) NULL DEFAULT NULL,
	`user_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK7ul8m5q12we515aa7b7ao0p44` (`budget_id`),
	INDEX `FKsg7jp0aj6qipr50856wf6vbw1` (`user_id`),
	CONSTRAINT `FK7ul8m5q12we515aa7b7ao0p44` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
	CONSTRAINT `FKsg7jp0aj6qipr50856wf6vbw1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);