
-- Users
INSERT INTO user (username, password) VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

-- Categories
INSERT INTO category (name, deleted) VALUES ('General', 0);
INSERT INTO category (name, deleted, user_id) VALUES ('Transport', 0, 1);

-- June 2021
INSERT INTO budget(start, end, tracking_period, volume, category_id, user_id) VALUES ('2021-06-01', '2021-07-01', 0, 600.0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-01 11:43:11', 14.00, 'Test transaction 01', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-02 14:29:17', 15.00, 'Test transaction 02', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-02 21:36:14', 12.45, 'Test transaction 03', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-03 21:58:30', 19.37, 'Test transaction 04', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-05 11:45:27', 18.15, 'Test transaction 05', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-07 19:31:06', 22.45, 'Test transaction 06', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-10 18:32:31', 2.56, 'Test transaction 07', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-11 14:44:14', 1.39, 'Test transaction 08', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-11 18:21:51', 57.49, 'Test transaction 09', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-11 21:24:17', 60.0, 'Test transaction 10', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-15 18:32:54', 78.23, 'Test transaction 11', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-16 08:23:51', 32.11, 'Test transaction 12', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-17 06:17:21', 29.99, 'Test transaction 13', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-18 17:58:18', 14.56, 'Test transaction 14', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-18 21:45:16', 10.11, 'Test transaction 15', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-19 11:15:16', 11.11, 'Test transaction 16', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-19 17:17:17', 27.34, 'Test transaction 17', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-20 12:37:32', 15.68, 'Test transaction 18', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-20 21:18:41', 47.99, 'Test transaction 19', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-21 09:15:00', 59.99, 'Test transaction 20', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-21 20:00:00', 178.34, 'Test transaction 21', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-21 22:22:41', 12.42, 'Test transaction 22', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-22 04:34:23', 17.72, 'Test transaction 23', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-22 15:18:12', 15.71, 'Test transaction 24', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-24 23:57:51', 13.00, 'Test transaction 25', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-25 13:51:31', 15.28, 'Test transaction 26', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-26 19:17:16', 14.34, 'Test transaction 27', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-27 13:13:12', 21.56, 'Test transaction 28', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-28 13:12:00', 55.99, 'Test transaction 29', 0, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-06-30 18:45:27', 3.29, 'Test transaction 30', 0, 1, 1);

-- July 2021
INSERT INTO budget(start, end, tracking_period, volume, category_id, user_id) VALUES ('2021-07-01', '2021-08-01', 0, 620, 1, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-02 13:25:10', 16.78, 'Test transaction 31', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-03 16:52:11', 25.00, 'Test transaction 32', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-04 20:38:34', 12.67, 'Test transaction 33', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-04 21:48:40', 7.49, 'Test transaction 34', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-06 09:15:56', 4.34, 'Test transaction 35', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-07 15:15:10', 2.57, 'Test transaction 36', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-09 19:42:11', 16.34, 'Test transaction 37', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-11 19:54:34', 24.79, 'Test transaction 38', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-11 23:31:11', 32.11, 'Test transaction 39', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-15 09:14:07', 11.11, 'Test transaction 40', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-16 17:25:10', 23.45, 'Test transaction 41', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-16 18:23:51', 21.32, 'Test transaction 42', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-17 19:39:24', 15.45, 'Test transaction 43', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-18 19:48:41', 15.50, 'Test transaction 44', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-18 22:15:36', 11.31, 'Test transaction 45', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-19 15:45:10', 12.89, 'Test transaction 46', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-20 16:17:11', 11.69, 'Test transaction 47', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-20 20:38:34', 19.87, 'Test transaction 48', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-20 21:28:00', 4.67, 'Test transaction 49', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-21 09:15:56', 13.99, 'Test transaction 50', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-21 20:00:00', 8.77, 'Test transaction 51', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-21 21:12:34', 2.00, 'Test transaction 52', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-22 07:45:31', 4.00, 'Test transaction 53', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-22 09:17:25', 34.31, 'Test transaction 54', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-25 16:59:59', 17.79, 'Test transaction 55', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-26 14:53:43', 14.45, 'Test transaction 56', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-26 15:31:19', 99.99, 'Test transaction 57', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-27 04:03:12', 42.42, 'Test transaction 58', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-27 04:31:42', 32.11, 'Test transaction 59', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-29 14:29:44', 17.67, 'Test transaction 60', 0, 2, 1);
INSERT INTO transaction (time, amount, comment, internal, budget_id, user_id) VALUES ('2021-07-29 22:18:38', 17.67, 'Test transaction 61', 0, 2, 1);