package com.spendingtrackerapi.dto;

import com.spendingtrackerapi.entity.BudgetEntity;
import lombok.Data;

import java.util.List;

@Data
public class BudgetInsertDto {
    private int year;
    private int month;
    private double volume;
    private BudgetEntity.TrackingPeriod trackingPeriod;
    private CategoryDto category;
    private List<TransactionInsertDto> transactions;
}