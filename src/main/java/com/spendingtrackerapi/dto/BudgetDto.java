package com.spendingtrackerapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.spendingtrackerapi.entity.BudgetEntity;
import com.spendingtrackerapi.util.CurrencyHelper;
import com.spendingtrackerapi.util.DateHelper;
import lombok.Data;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class BudgetDto {
    private int id;
    private int year;
    private int month;
    private double volume;
    private BudgetEntity.TrackingPeriod trackingPeriod;
    private CategoryDto category;
    private List<TransactionDto> transactions;

    public double getDailyVolume() {
        DateHelper.PeriodDetails periodDetails = DateHelper.getPeriod(getYear(), getMonth());
        return CurrencyHelper.round(getVolume() / periodDetails.getDays());
    }
}