package com.spendingtrackerapi.dto;

import lombok.Data;

@Data
public class TransactionDto extends TransactionInsertDto {
    private int id;
}