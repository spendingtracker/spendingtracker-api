package com.spendingtrackerapi.dto;

import lombok.Data;

@Data
public class CategoryDto extends CategoryInsertDto {
    private int id;
    private boolean deleted;
}