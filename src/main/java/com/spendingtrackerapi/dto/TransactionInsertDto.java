package com.spendingtrackerapi.dto;

import com.spendingtrackerapi.entity.TransactionEntity;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TransactionInsertDto {
    private LocalDateTime time;
    private double amount;
    private String comment;
    private boolean internal;
    private TransactionEntity.Status status;
    private int budgetId;
    private boolean template;
}