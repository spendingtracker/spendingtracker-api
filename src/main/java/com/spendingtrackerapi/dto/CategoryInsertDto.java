package com.spendingtrackerapi.dto;

import lombok.Data;

@Data
public class CategoryInsertDto {
    private String name;
    private boolean ownedByUser;
}