package com.spendingtrackerapi.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class DateHelper {

    public static LocalDate getCurrentMonthStart() {
        return LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), 1);
    }

    public static LocalDate getCurrentMonthEnd() {
        return getCurrentMonthStart().plusMonths(1);
    }

    public static long getCurrentMonthDayCount() {
        return ChronoUnit.DAYS.between(getCurrentMonthStart(), getCurrentMonthEnd());
    }

    public static PeriodDetails getPeriod(int year, int month) {
        LocalDate start = LocalDate.of(year, month, 1);
        LocalDate end = start.plusMonths(1L);
        return new PeriodDetails(start, end);
    }

    @Getter
    @RequiredArgsConstructor
    public static class PeriodDetails {
        private final LocalDate start;
        private final LocalDate end;

        public long getDays() {
            return ChronoUnit.DAYS.between(getStart(), getEnd());
        }

        public long getPassedDays() {
            LocalDate now = LocalDate.now();
            if (now.isBefore(getStart())) {
                return 0;
            } else if (now.isBefore(getEnd())) {
                return ChronoUnit.DAYS.between(getStart(), now) + 1;
            } else {
                return getDays();
            }
        }

        public long getRemainingDays() {
            return getDays() - getPassedDays();
        }
    }
}