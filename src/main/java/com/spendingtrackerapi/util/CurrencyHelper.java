package com.spendingtrackerapi.util;

public class CurrencyHelper {

    public static double round(double amount) {
        return Math.round(amount * 100.0) / 100.0;
    }
}