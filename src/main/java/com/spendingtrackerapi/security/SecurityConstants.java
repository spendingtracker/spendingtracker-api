package com.spendingtrackerapi.security;

public class SecurityConstants {
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_EXPIRES = "Expires";
    public static final String SIGN_UP_URL = "/users/sign-up";
}