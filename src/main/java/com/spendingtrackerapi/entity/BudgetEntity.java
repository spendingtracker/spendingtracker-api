package com.spendingtrackerapi.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

import static com.spendingtrackerapi.entity.ProtectedEntity.EntityType.BUDGET;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "budget")
@Data
public class BudgetEntity extends ProtectedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDate start;
    private LocalDate end;
    private double volume;
    private TrackingPeriod trackingPeriod;
    @ManyToOne(fetch = FetchType.LAZY)
    private CategoryEntity category;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "budget")
    @OrderBy("time desc")
    private List<TransactionEntity> transactions;
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public EntityType getEntityType() {
        return BUDGET;
    }

    public enum TrackingPeriod {
        DAILY, MONTHLY
    }
}