package com.spendingtrackerapi.entity;

public abstract class ProtectedEntity {
    public abstract int getId();
    public abstract EntityType getEntityType();

    public enum EntityType {
        BUDGET, TRANSACTION, CATEGORY
    }
}