package com.spendingtrackerapi.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "transaction")
@Data
public class TransactionEntity extends ProtectedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDateTime time;
    private double amount;
    private String comment;
    private boolean internal;
    private Status status;
    private boolean template;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "budget_id")
    private BudgetEntity budget;
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.TRANSACTION;
    }

    public enum Status {
        APPROVED, PENDING
    }
}