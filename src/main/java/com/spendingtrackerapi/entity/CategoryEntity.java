package com.spendingtrackerapi.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "category")
@Data
public class CategoryEntity extends ProtectedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private boolean deleted;
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.CATEGORY;
    }
}