package com.spendingtrackerapi.service;

import com.spendingtrackerapi.entity.*;
import com.spendingtrackerapi.repository.BudgetRepository;
import com.spendingtrackerapi.repository.CategoryRepository;
import com.spendingtrackerapi.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorizationService {
    private final UserService userService;
    private final BudgetRepository budgetRepository;
    private final TransactionRepository transactionRepository;
    private final CategoryRepository categoryRepository;

    public boolean isAllowedToAccess(ProtectedEntity protectedEntity) {
        UserEntity authenticatedUser = userService.getAuthenticatedUser();

        switch (protectedEntity.getEntityType()) {
            case BUDGET:
                Optional<BudgetEntity> budget = budgetRepository.findById(protectedEntity.getId());
                return budget.isPresent() && budget.get().getUser().getId() == authenticatedUser.getId();
            case TRANSACTION:
                Optional<TransactionEntity> transaction = transactionRepository.findById(protectedEntity.getId());
                return transaction.isPresent() && transaction.get().getUser().getId() == authenticatedUser.getId();
            case CATEGORY:
                Optional<CategoryEntity> category = categoryRepository.findById(protectedEntity.getId());
                return category.isPresent() && category.get().getUser().getId() == authenticatedUser.getId();
            default:
                return false;
        }
    }
}