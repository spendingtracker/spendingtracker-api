package com.spendingtrackerapi.service;

import com.spendingtrackerapi.dto.CategoryDto;
import com.spendingtrackerapi.dto.CategoryInsertDto;
import com.spendingtrackerapi.entity.CategoryEntity;
import com.spendingtrackerapi.entity.UserEntity;
import com.spendingtrackerapi.repository.CategoryRepository;
import com.spendingtrackerapi.transformer.CategoryTransformer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryTransformer categoryTransformer;
    private final UserService userService;
    private final AuthorizationService authorizationService;

    public List<CategoryDto> getCategories() {
        UserEntity userEntity = userService.getAuthenticatedUser();
        return categoryRepository.findByUserAsc(userEntity).stream()
                .map(categoryTransformer::toDto).collect(Collectors.toList());
    }

    public void addCategory(CategoryInsertDto categoryDto) {
        CategoryEntity categoryEntity = categoryTransformer.toNewEntity(categoryDto);
        categoryRepository.save(categoryEntity);
    }

    public void saveCategory(CategoryDto categoryDto) {
        CategoryEntity categoryEntity = categoryTransformer.toExistingEntity(categoryDto);
        if (authorizationService.isAllowedToAccess(categoryEntity)) {
            categoryRepository.save(categoryEntity);
        }
    }

    public void deleteCategory(int categoryId) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(categoryId);
        if (categoryEntity.isPresent()) {
            if (authorizationService.isAllowedToAccess(categoryEntity.get())) {
                if (categoryRepository.budgetsExist(categoryEntity.get().getId())) {
                    categoryEntity.get().setDeleted(true);
                    categoryRepository.save(categoryEntity.get());
                } else {
                    categoryRepository.delete(categoryEntity.get());
                }
            }
        }
    }

    public void restoreCategory(int categoryId) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(categoryId);
        if (categoryEntity.isPresent()) {
            if (authorizationService.isAllowedToAccess(categoryEntity.get())) {
                categoryEntity.get().setDeleted(false);
                categoryRepository.save(categoryEntity.get());
            }
        }
    }
}