package com.spendingtrackerapi.service;

import com.spendingtrackerapi.dto.TransactionDto;
import com.spendingtrackerapi.dto.TransactionInsertDto;
import com.spendingtrackerapi.entity.TransactionEntity;
import com.spendingtrackerapi.repository.TransactionRepository;
import com.spendingtrackerapi.transformer.TransactionTransformer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TransactionService {
    private final TransactionRepository transactionRepository;
    private final UserService userService;
    private final AuthorizationService authorizationService;
    private final TransactionTransformer transactionTransformer;

    public void addTransactions(TransactionInsertDto[] transactionDtos) {
        if (transactionDtos != null) {
            for (TransactionInsertDto transaction : transactionDtos) {
                this.addTransaction(transaction);
            }
        }
    }

    public void saveTransactions(TransactionDto[] transactionDtos) {
        for (TransactionDto transaction : transactionDtos) {
            this.saveTransaction(transaction);
        }
    }

    public void addTransaction(TransactionInsertDto transactionDto) {
        TransactionEntity transactionEntity = transactionTransformer.toNewEntity(transactionDto);
        transactionRepository.save(transactionEntity);
    }

    public void saveTransaction(TransactionDto transactionDto) {
        TransactionEntity transactionEntity = transactionTransformer.toExistingEntity(transactionDto);
        if (authorizationService.isAllowedToAccess(transactionEntity)) {
            transactionRepository.save(transactionEntity);
        }
    }

    public void deleteTransaction(int id) {
        TransactionEntity transactionEntity = transactionRepository.getOne(id);
        if (authorizationService.isAllowedToAccess(transactionEntity)) {
            transactionRepository.delete(transactionEntity);
        }
    }
}