package com.spendingtrackerapi.service;

import com.spendingtrackerapi.dto.BudgetDto;
import com.spendingtrackerapi.dto.BudgetInsertDto;
import com.spendingtrackerapi.entity.BudgetEntity;
import com.spendingtrackerapi.entity.TransactionEntity;
import com.spendingtrackerapi.entity.UserEntity;
import com.spendingtrackerapi.repository.BudgetRepository;
import com.spendingtrackerapi.repository.TransactionRepository;
import com.spendingtrackerapi.transformer.BudgetTransformer;
import com.spendingtrackerapi.transformer.TransactionTransformer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BudgetService {
    private final BudgetRepository budgetRepository;
    private final TransactionRepository transactionRepository;
    private final AuthorizationService authorizationService;
    private final BudgetTransformer budgetTransformer;
    private final TransactionTransformer transactionTransformer;
    private final UserService userService;

    public List<BudgetDto> getBudgets(int year, int month) {
        UserEntity userEntity = userService.getAuthenticatedUser();
        LocalDate from = LocalDate.of(year, month, 1);
        LocalDate to = from.plusMonths(1);
        return budgetRepository.findByPeriodOrderByTimeDesc(from, to, userEntity).stream()
                .map(budgetTransformer::toDto).collect(Collectors.toList());
    }

    public List<BudgetDto> getBudgets(int yearFrom, int monthFrom, int yearTo, int monthTo) {
        LocalDate from = LocalDate.of(yearFrom, monthFrom, 1);
        LocalDate to = LocalDate.of(yearTo, monthTo, 1).plusMonths(1);
        UserEntity userEntity = userService.getAuthenticatedUser();
        return budgetRepository.findByPeriodOrderByTimeDesc(from, to, userEntity).stream()
                .map(budgetTransformer::toDto).collect(Collectors.toList());
    }

    public void addBudget(BudgetInsertDto budgetDto) {
        BudgetEntity budgetEntity = budgetTransformer.toNewEntity(budgetDto);
        final BudgetEntity savedBudgetEntity = budgetRepository.save(budgetEntity);
        if (budgetDto.getTransactions() != null) {
            List<TransactionEntity> transactions = budgetDto.getTransactions().stream()
                    .map(transaction -> {
                        transaction.setBudgetId(savedBudgetEntity.getId());
                        return transactionTransformer.toNewEntity(transaction);
                    }).collect(Collectors.toList());
            transactionRepository.saveAll(transactions);
        }
    }

    public void saveBudget(BudgetDto budgetDto) {
        BudgetEntity budgetEntity = budgetTransformer.toExistingEntity(budgetDto);
        if (authorizationService.isAllowedToAccess(budgetEntity)) {
            final BudgetEntity savedBudgetEntity = budgetRepository.save(budgetEntity);
            if (budgetDto.getTransactions() != null) {
                List<TransactionEntity> transactions = budgetDto.getTransactions().stream()
                        .map(transaction -> {
                            transaction.setBudgetId(savedBudgetEntity.getId());
                            return transactionTransformer.toExistingEntity(transaction);
                        }).collect(Collectors.toList());
                transactionRepository.saveAll(transactions);
            }
        }
    }

    public void addBudgets(BudgetInsertDto[] budgetDtos) {
        if (budgetDtos != null) {
            for (BudgetInsertDto budget : budgetDtos) {
                this.addBudget(budget);
            }
        }
    }

    public void saveBudgets(BudgetDto[] budgetDtos) {
        for (BudgetDto budget : budgetDtos) {
            this.saveBudget(budget);
        }
    }
}