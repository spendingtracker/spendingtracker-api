package com.spendingtrackerapi.repository;

import com.spendingtrackerapi.entity.CategoryEntity;
import com.spendingtrackerapi.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Integer> {
    @Query("SELECT c FROM category c WHERE c.user = :user OR c.user is null ORDER BY c.user ASC, c.name ASC")
    List<CategoryEntity> findByUserAsc(UserEntity user);

    @Query("SELECT COUNT(b.id) > 0 FROM budget b WHERE b.category.id = :budgetId")
    boolean budgetsExist(int budgetId);
}