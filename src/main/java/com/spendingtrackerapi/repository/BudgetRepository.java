package com.spendingtrackerapi.repository;

import com.spendingtrackerapi.entity.BudgetEntity;
import com.spendingtrackerapi.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface BudgetRepository extends JpaRepository<BudgetEntity, Integer> {
    @Query("SELECT b FROM budget b WHERE b.start < :date_to AND b.end > :date_from AND b.user = :user ORDER BY b.start ASC, b.end ASC")
    List<BudgetEntity> findByPeriodOrderByTimeDesc(@Param("date_from") LocalDate dateFrom, @Param("date_to") LocalDate dateTo, UserEntity user);
}