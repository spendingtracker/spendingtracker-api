package com.spendingtrackerapi.controller;

import com.spendingtrackerapi.dto.BudgetDto;
import com.spendingtrackerapi.dto.BudgetInsertDto;
import com.spendingtrackerapi.service.BudgetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/budgets")
@RequiredArgsConstructor
public class BudgetController {
    private final BudgetService budgetService;

    @GetMapping("/from/{yearFrom}/{monthFrom}/to/{yearTo}/{monthTo}")
    public List<BudgetDto> getBudgets(@PathVariable int yearFrom, @PathVariable int monthFrom,
                                      @PathVariable int yearTo, @PathVariable int monthTo) {
        return budgetService.getBudgets(yearFrom, monthFrom, yearTo, monthTo);
    }

    @GetMapping("/{year}/{month}")
    public List<BudgetDto> getBudgets(@PathVariable int year, @PathVariable int month) {
        return budgetService.getBudgets(year, month);
    }

    @PostMapping("/budget")
    public void addBudget(@RequestBody BudgetInsertDto budget) {
        budgetService.addBudget(budget);
    }

    @PutMapping("/budget")
    public void saveBudget(@RequestBody BudgetDto budget) {
        budgetService.saveBudget(budget);
    }

    @PostMapping
    public void addBudgets(@RequestBody BudgetInsertDto[] budgets) {
        budgetService.addBudgets(budgets);
    }

    @PutMapping
    public void saveBudgets(@RequestBody BudgetDto[] budgets) {
        budgetService.saveBudgets(budgets);
    }
}