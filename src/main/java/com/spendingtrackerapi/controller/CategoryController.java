package com.spendingtrackerapi.controller;

import com.spendingtrackerapi.dto.CategoryDto;
import com.spendingtrackerapi.dto.CategoryInsertDto;
import com.spendingtrackerapi.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping
    public List<CategoryDto> getCategories() {
        return categoryService.getCategories();
    }

    @PostMapping("/category")
    public void addCategory(@RequestBody CategoryInsertDto category) {
        categoryService.addCategory(category);
    }

    @PutMapping("/category")
    public void saveCategory(@RequestBody CategoryDto category) {
        categoryService.saveCategory(category);
    }

    @DeleteMapping("/category/{categoryId}")
    public void deleteCategory(@PathVariable int categoryId) {
        categoryService.deleteCategory(categoryId);
    }

    @PutMapping("/category/{categoryId}/restore")
    public void restoreCategory(@PathVariable int categoryId) {
        categoryService.restoreCategory(categoryId);
    }
}