package com.spendingtrackerapi.controller;

import com.spendingtrackerapi.dto.TransactionDto;
import com.spendingtrackerapi.dto.TransactionInsertDto;
import com.spendingtrackerapi.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transactions")
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;

    @PostMapping("/transaction")
    public void addTransaction(@RequestBody TransactionInsertDto transaction) {
        transactionService.addTransaction(transaction);
    }

    @PutMapping("/transaction")
    public void saveTransaction(@RequestBody TransactionDto transaction) {
        transactionService.saveTransaction(transaction);
    }

    @PostMapping
    public void addTransactions(@RequestBody TransactionInsertDto[] transactions) {
        transactionService.addTransactions(transactions);
    }

    @PutMapping
    public void saveTransactions(@RequestBody TransactionDto[] transactions) {
        transactionService.saveTransactions(transactions);
    }

    @DeleteMapping("/transaction/{id}")
    public void deleteTransaction(@PathVariable int id) {
        transactionService.deleteTransaction(id);
    }
}