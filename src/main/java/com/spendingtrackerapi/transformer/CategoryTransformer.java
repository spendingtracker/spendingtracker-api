package com.spendingtrackerapi.transformer;

import com.spendingtrackerapi.dto.CategoryDto;
import com.spendingtrackerapi.dto.CategoryInsertDto;
import com.spendingtrackerapi.entity.CategoryEntity;
import com.spendingtrackerapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CategoryTransformer {
    private final UserService userService;

    public CategoryDto toDto(CategoryEntity categoryEntity) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(categoryEntity.getId());
        categoryDto.setName(categoryEntity.getName());
        categoryDto.setDeleted(categoryEntity.isDeleted());
        categoryDto.setOwnedByUser(categoryEntity.getUser() != null);
        return categoryDto;
    }

    public CategoryEntity toNewEntity(CategoryInsertDto categoryDto) {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setName(categoryDto.getName());
        categoryEntity.setUser(userService.getAuthenticatedUser());
        return categoryEntity;
    }

    public CategoryEntity toExistingEntity(CategoryDto categoryDto) {
        CategoryEntity categoryEntity = this.toNewEntity(categoryDto);
        categoryEntity.setId(categoryDto.getId());
        categoryEntity.setDeleted(categoryDto.isDeleted());
        return categoryEntity;
    }
}