package com.spendingtrackerapi.transformer;

import com.spendingtrackerapi.dto.TransactionDto;
import com.spendingtrackerapi.dto.TransactionInsertDto;
import com.spendingtrackerapi.entity.TransactionEntity;
import com.spendingtrackerapi.repository.BudgetRepository;
import com.spendingtrackerapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TransactionTransformer {
    private final UserService userService;
    private final BudgetRepository budgetRepository;

    public TransactionEntity toNewEntity(TransactionInsertDto transactionDto) {
        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setAmount(transactionDto.getAmount());
        transactionEntity.setComment(transactionDto.getComment());
        transactionEntity.setInternal(transactionDto.isInternal());
        transactionEntity.setStatus(transactionDto.getStatus());
        transactionEntity.setTemplate(transactionDto.isTemplate());
        transactionEntity.setTime(transactionDto.getTime());
        transactionEntity.setUser(userService.getAuthenticatedUser());
        transactionEntity.setBudget(budgetRepository.getOne(transactionDto.getBudgetId()));
        return transactionEntity;
    }

    public TransactionEntity toExistingEntity(TransactionDto transactionDto) {
        TransactionEntity transactionEntity = this.toNewEntity(transactionDto);
        transactionEntity.setId(transactionDto.getId());
        return transactionEntity;
    }

    public TransactionDto toDto(TransactionEntity transactionEntity) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setId(transactionEntity.getId());
        transactionDto.setAmount(transactionEntity.getAmount());
        transactionDto.setComment(transactionEntity.getComment());
        transactionDto.setInternal(transactionEntity.isInternal());
        transactionDto.setStatus(transactionEntity.getStatus());
        transactionDto.setTemplate(transactionEntity.isTemplate());
        transactionDto.setTime(transactionEntity.getTime());
        transactionDto.setBudgetId(transactionEntity.getBudget().getId());
        return transactionDto;
    }
}