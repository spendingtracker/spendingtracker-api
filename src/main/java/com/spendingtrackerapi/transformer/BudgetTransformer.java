package com.spendingtrackerapi.transformer;

import com.spendingtrackerapi.dto.BudgetDto;
import com.spendingtrackerapi.dto.BudgetInsertDto;
import com.spendingtrackerapi.entity.BudgetEntity;
import com.spendingtrackerapi.service.UserService;
import com.spendingtrackerapi.util.CurrencyHelper;
import com.spendingtrackerapi.util.DateHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BudgetTransformer {
    private final UserService userService;
    private final TransactionTransformer transactionTransformer;
    private final CategoryTransformer categoryTransformer;

    public BudgetEntity toNewEntity(BudgetInsertDto budgetDto) {
        DateHelper.PeriodDetails periodDetails = DateHelper.getPeriod(budgetDto.getYear(), budgetDto.getMonth());
        BudgetEntity budgetEntity = new BudgetEntity();
        budgetEntity.setVolume(CurrencyHelper.round(budgetDto.getVolume()));
        budgetEntity.setTrackingPeriod(budgetDto.getTrackingPeriod());
        budgetEntity.setStart(periodDetails.getStart());
        budgetEntity.setEnd(periodDetails.getEnd());
        budgetEntity.setCategory(categoryTransformer.toExistingEntity(budgetDto.getCategory()));
        budgetEntity.setUser(userService.getAuthenticatedUser());
        return budgetEntity;
    }

    public BudgetEntity toExistingEntity(BudgetDto budgetDto) {
        DateHelper.PeriodDetails periodDetails = DateHelper.getPeriod(budgetDto.getYear(), budgetDto.getMonth());
        BudgetEntity budgetEntity = new BudgetEntity();
        budgetEntity.setId(budgetDto.getId());
        budgetEntity.setVolume(CurrencyHelper.round(budgetDto.getVolume()));
        budgetEntity.setTrackingPeriod(budgetDto.getTrackingPeriod());
        budgetEntity.setStart(periodDetails.getStart());
        budgetEntity.setEnd(periodDetails.getEnd());
        budgetEntity.setCategory(categoryTransformer.toExistingEntity(budgetDto.getCategory()));
        budgetEntity.setUser(userService.getAuthenticatedUser());
        return budgetEntity;
    }

    public BudgetDto toDto(BudgetEntity budgetEntity) {
        BudgetDto budgetDto = new BudgetDto();
        budgetDto.setId(budgetEntity.getId());
        budgetDto.setYear(budgetEntity.getStart().getYear());
        budgetDto.setMonth(budgetEntity.getStart().getMonth().getValue());
        budgetDto.setVolume(budgetEntity.getVolume());
        budgetDto.setTrackingPeriod(budgetEntity.getTrackingPeriod());
        budgetDto.setCategory(categoryTransformer.toDto(budgetEntity.getCategory()));
        budgetDto.setTransactions(budgetEntity.getTransactions().stream()
                .map(transactionTransformer::toDto).collect(Collectors.toList()));
        return budgetDto;
    }
}