
# Project Overview

This piece of software is the API part of the spendingtracker system developed for tracking running expenses in order to answer to the following questions:
  + Are you are spending more than expected?
  + Are you are spending faster than expected?

# Installation and Running Instructions

Please go through the following instructions in order to set up the development environment.

## Database Setup

##### Connect with the MySQL/MariaDB server
~~~
sudo mysql -u root -p
~~~

##### Create DB user
~~~
create user 'spendingtracker'@'localhost' identified by 'tere';
~~~

##### Create DB
~~~
CREATE DATABASE spendingtrackerdev CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
~~~

##### Grant privileges to the DB user
~~~
grant all privileges on spendingtrackerdev.* to 'spendingtracker'@'localhost';
~~~

## Installation of Dependencies, Build

~~~
./mvnw clean install (Linux)
mvnw.bat clean install (Windows)
~~~

## Running of Application

Navigate to project root folder and then...
~~~
./mvnw spring-boot:run (Linux)
mvnw.bat spring-boot:run (Windows)
~~~

The REST-service is available at
http://localhost:8080/

Default credentials for log in:
+ Username: admin
+ Password: password

*NB! Make sure you have this spendingtracker API set up and running in your localhost before starting up the client part of the software (available [here](https://gitlab.com/spendingtracker/spendingtracker-client)).